class CfgMagazineWells
{
    class CBA_12g_8rnds 
    {
        FFAA_Mags[] =
        {
            //8 round 12 gauge shells (Fabarm SDASS)
            "ffaa_8Rnd_12Gauge_Slug",
            "ffaa_8Rnd_12Gauge_Pellets"
        };
    };
    class CBA_50BMG_M107
    {
        FFAA_Mags[] =
        {
            //10 round AW 50 | Barrett M95 mags
            "ffaa_127x99_ap", //AP
            "ffaa_127x99_he" //HE
        };
    };
    class CBA_556x45_G36
    {
        FFAA_Mags[] =
        {
            //30 round G36 mags
            "ffaa_556x45_g36",
            "ffaa_556x45_g36_tracer_green"
        }
    };
    class CBA_556x45_STANAG
    {
        FFAA_mags[] =
        {
            //30 round Cetme L/LC mag
            "ffaa_556x45_cedmel"
        };
    };
    class CBA_556x45_MINIMI
    {
        FFAA_belts[] =
        {
            //200 round Ameli box
            "ffaa_556x45_ameli"
        };
    };
    class CBA_57x28_P90
    {
        FFAA_Mags[] =
        {
            //50 round P90 mag
            "ffaa_507x28_p90"
        };
    };
    class CBA_762x51_G3
    {
        FFAA_Mags[] =
        {
            //20 round Cetme C/E mag
            "ffaa_762x51_cedmec"
        };
    };
    class CBA_762x51_AICS
    {
        FFAA_Mags[] =
        {
            //10 round L96A1 mag
            "ffaa_762x51_accuracy"
        };
    };
    class CBA_762x51_LINKS
    {
        FFAA_belts[] =
        {
            //200 round MG4 mag
            "ffaa_556x45_mg4"
        };
    };
    class CBA_792x57_LINKS
    {
       FFAA_belts[] =
        {
            //100 round MG-42 mag
            "ffaa_762x51_mg3"
        };
    };
    class CBA_9x19_MP5
    {
        FFAA_Mags[] =
        {
            //30 round MP5 mag
            "ffaa_9x19_mp5"
        };
    };
    class CBA_9x19_UMP
    {
        FFAA_Mags[] =
        {
            //30 round UMP mag
            "ffaa_9x19_ump"
        };
    };
};
