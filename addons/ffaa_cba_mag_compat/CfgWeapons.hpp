class CfgWeapons
{
	class Rifle_Base_F;
    class Pistol_Base_F;
    //Ameli
    class ffaa_armas_ameli: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_MINIMI"};
    };
    //L96A1
    class ffaa_armas_aw: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x51_AICS"};
    };
    //Barrett M95, AW 50
    class ffaa_armas_m95: ffaa_armas_aw
    {
        magazineWell[] = {"CBA_50BMG_M107"}; //= instead of += because inheriting from other calibre weapon
    };
    //Cetme C
    class ffaa_armas_cetme_c: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x51_G3","CBA_762x51_G3_L","CBA_762x51_G3_XL"};
    };
    //Cetme L
    class ffaa_armas_cetme_l: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_STANAG"};
    };
    //Fabarm SDASS
    class ffaa_armas_sdass: Rifle_Base_F
    {
        magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds","CBA_12g_6rnds","CBA_12g_7rnds","CBA_12g_8rnds"};
    };
    //HK G36
    class ffaa_armas_hkg36e_normal: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_G36"};
    };
    //HK MP5
    class ffaa_armas_hkmp510a3: Rifle_Base_F
    {
        magazineWell[] += {"CBA_9x19_MP5"};
    };
    //HK UMP
    class ffaa_armas_hkmp5a5;
    class ffaa_armas_ump: ffaa_armas_hkmp5a5
    {
        magazineWell[] += {"CBA_9x19_UMP"};
    };
    //HK USP, FNP 9, Sig Sauer P226
    class ffaa_armas_usp: Pistol_Base_F
    {
        magazineWell[] += {"CBA_9x19_P226"};
    };
    //MG4
    class ffaa_armas_mg4: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_MINIMI"};
    };
    //MG-42, MG-3
    class ffaa_armas_mg3: Rifle_Base_F
    {
        magazineWell[] += {"CBA_792x57_LINKS"};
    };
    //P90
    class ffaa_armas_p90: Rifle_Base_F
    {
        magazineWell[] += {"CBA_57x28_P90"};
    };
};
