class CfgPatches
{
    class armaforces_ffaa_cba_mag_compat
    {
        name = "ArmaForces - FFAA CBA Mag Compat";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"ffaa_armas", "cba_main"};
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
