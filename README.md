# ArmaForces - FFAA CBA Mag Compat

[Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1651148102)

## Content

This addons brings CBA Magwell compatibility to FFAA Weapons and Magazines.

[Arsenal Gallery](https://imgur.com/a/hMBQY8t)

## Development environment

It is recommended to use Visual Studio Code with following extensions:
 * [SQFLint](https://marketplace.visualstudio.com/items?itemName=skacekachna.sqflint) - SQF Linting and completion

For addon building it is reccommended to use [armake2](https://github.com/KoffeinFlummi/armake2).

The repository contains `build.bat` file that will use armake2 from "tools" dir to build the addon.  
If `ArmaForces_FFAA_CBA_Mag_Compat.biprivatekey` will be present in root of the repo, then built PBOs will be signed with this key.
